import * as React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import ErrorBoundary from './common/ErrorBoundary';

const Home = React.lazy(() => import('./pages/Home'));
const NotFound = React.lazy(() => import('./pages/404'));

const App: React.FC = () => (
  <Router>
    <h1>This is an example UI</h1>
    <div>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/profile">Profile</Link>
          </li>
        </ul>
      </nav>

      <ErrorBoundary>
        <React.Suspense fallback={<h1>Loading...</h1>}>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route>
              <NotFound />
            </Route>
          </Switch>
        </React.Suspense>
      </ErrorBoundary>
    </div>
  </Router>
);
export default App;
