// Hot reloading
const hotReload = module && (module as any).hot;
if (hotReload) {
  hotReload.accept();
}