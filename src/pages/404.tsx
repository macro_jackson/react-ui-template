import * as React from 'react';

const NotFoundPage: React.FC = () => <h1>Page Not Found</h1>;
export default NotFoundPage;
